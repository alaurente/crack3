#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;          // Maximum any password can be
const int HASH_LEN=33;          // Length of MD5 hash strings
const int LINE_LEN=1000;        // Maximum length of string line
const int DICTIONARY_LINES=2000; // Number of lines on dictionary


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char *password;
    char *md5hash;
};


// Read in the dictionary file and return an array of structs.
// Each entry should contain both the hash and the dictionary
// word.
struct entry *read_dictionary(char *filename, int *size)
{
    FILE *f = fopen(filename,"r'");
    char line [LINE_LEN];
    int count = 0;
    int lines = DICTIONARY_LINES;
    struct entry *words = malloc(lines * sizeof(struct entry));

    // File not found, return NULL with size 0
    if (f == NULL) {
        *size = 0;
        return NULL;
    }

    // File found, process it
    while (fgets(line, LINE_LEN, f) !=NULL)
    {
        if (count == lines)
        {
            lines += DICTIONARY_LINES;
            words = realloc(words, lines * sizeof(char *));
        }
        line[strlen(line)-1] = '\0';
        char *word = malloc(strlen(line) * sizeof(char) + 1);

        strcpy(word, line);
        words[count].password = word;
        
        char *hashString = md5(word, strlen(word));
        words[count].md5hash = hashString;
        
        count++;
        
        // ***** TEST *****
        // printf("%s %s", word, hashString);
        // ****************
    }
    
    // Close the file
    fclose(f);

    *size = count;
    return words;    

}

// Comparator function for qsort routine
int comp_by_hash(const void *a, const void *b)
{
    struct entry *ha = (struct entry *)a;
    struct entry *hb = (struct entry *)b;

    return strcmp(ha->md5hash, (*hb).md5hash);    
    
}

// Comparator function for bsearch routine
int cbcomp(const void *target, const void *elem)
{
    char *target_str = (char *)target;
    struct entry *celem = (struct entry *)elem;
    return strcmp(target_str, (*celem).md5hash);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int dlen, size;
    char hashline[LINE_LEN];
    char *hashString;
    
    // Read the dictionary file into an array of entry structures
    struct entry *dict = read_dictionary(argv[2], &dlen);
    size = DICTIONARY_LINES;

    // Sort the hashed dictionary using qsort.
    qsort(dict, size, sizeof(struct entry), comp_by_hash);

    // ***** TEST *****
    //for (i=0; i<DICTIONARY_LINES; i++)
    //    printf("%s\n", dict[i].md5hash);
    // ****************

    // Open the hash file for reading.
    FILE *h = fopen(argv[1],"r'");

    // For each hash, try every entry in the dictionary.
    while (fgets(hashline, sizeof hashline, h) !=NULL)
    {
        hashline[strlen(hashline)-1] = '\0';
        hashString = malloc(strlen(hashline) * sizeof(char) +1);
        strcpy(hashString, hashline);

        // For each hash, search for it in the dictionary using binary search.        
        struct entry *found = bsearch(hashString, dict, dlen, sizeof(struct entry), cbcomp);
        if (found == NULL)
        {
            printf("Not found\n");
        }
        else
        {
            // If you find it, get the corresponding plaintext dictionary word.
            // Print out both the hash and word.            
            printf("Found %s %s\n", found->password, found->md5hash);
        }
    }
    
    // Close the file
    fclose(h); 
    
}
